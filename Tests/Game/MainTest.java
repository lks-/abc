package Game;

import org.junit.Test;

import static Game.Main.sum;
import static org.junit.Assert.*;

public class MainTest {

    @Test
    public void main() throws Exception {
        assertEquals(10,sum(2,5));
        assertEquals(0,sum(0,5));
        assertEquals(0,sum(2,0));
        assertEquals(0,sum(0,0));
        assertEquals(-10,sum(-2,5));
        assertEquals(-10,sum(2,-5));
        assertEquals(10,sum(-2,-5));
    }
}