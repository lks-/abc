package Game;

public class Chocolate {
    public static void main(String[] args) {
        int money = 15;
        int wrap = 3;
        int chocolate = 0;
        for (int i = 0; i < money; i++) {
            chocolate++;
            if (chocolate % wrap == 0) {
                chocolate++;
            }
        }
        System.out.println(chocolate);
    }
}
