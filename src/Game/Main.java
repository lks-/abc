package Game;

import static java.lang.Math.abs;

/**
 * Умножение без умноженя
 *
 * @author A. Lukashevich 17it18
 */
public class Main {
    public static void main(String[] args) {
        int num1 = 2, num2 = 3, change = 0;  //change - Вспомогательная переменная для свапа
        if (num1 == 0 || num2 == 0) {
            System.out.println(0);
            System.exit(1);
        }
        if (num1 < num2) {
            change = num1;
            num1 = num2;
            num2 = change;
            System.out.println(sum(num1, num2));
        } else {
            System.out.println(sum(num1, num2));
        }
    }

    /**
     * Метод который умножает
     *
     * @param num1 - Первый множитель
     * @param num2 - Второй множитель
     * @return - Результат
     */
    static private int sum(int num1, int num2) {
        int result = 0;
        if (num1 < 0 || num2 < 0) {
            for (int i = 0; i < abs(num2); i++) {
                result -= num1;
            }
            return result;
        } else {
            for (int i = 0; i < num2; i++) {
                result += num1;
            }
            return result;
        }
    }
}