package Game;


/**
 * Сортировка по колличску повторение
 *
 * @author A. Lukashevich 17it18
 */
public class Sort {
    public static void main(String args[]) {
        //int[] array = {8, 5, 2, 2, 5, 6, 8, 8};
        int[] array = {2, 5, 2, 6, -1, 99999, 5, 8, 8, 8};
        sortByCount(array);
        printArray(array);
    }

    /**
     *  Определение колличества повторение элементов
     *
     * @param arr - массив
     * @param n - элемет массива
     *
     * @return - возврат колличество повторений эллемента в масииве
     */
    static int elementsCount(int arr[], int n) {
        int count = 0;
        for (int i : arr) {
            if (i == n) {
                count++;
            }
        }
        return count;
    }

    /**
     * Сортировка по колличесву эллементов
     *
     * @param arr - массив
     */
    static void sortByCount(int arr[]) {
        int temp = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (elementsCount(arr, arr[i]) < elementsCount(arr, arr[j])) {
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;

                }
                else if(elementsCount(arr,arr[i]) == elementsCount(arr,arr[j])){
                    temp = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = temp;
                }
            }
        }
    }

    //Вывести масив
    static void printArray(int[] ar) {
        for (int i : ar) {
            System.out.print(i + ", ");
        }
    }
}