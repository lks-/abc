package ru.lae.tenThreath;

/**
 * Создание 10ти потоков
 *
 * @author Lukashevich A.
 */
public class Main {
    public static void main(String[] args) {
        Threath threath;
        for (int i = 1; i <= 10; i++) {
            threath = new Threath("Поток " + i);
            threath.start();
            System.out.println("Поток " + threath + " создан");
        }
    }
}
