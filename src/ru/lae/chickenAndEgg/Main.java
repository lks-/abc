package ru.lae.chickenAndEgg;

/**
 * Спор двух потоков
 *
 * @author Lukashevich A.
 */
public class Main {
    static Threat chiken;

    public static void main(String[] args) {
        chiken = new Threat();
        System.out.println("Спор начат...");
        chiken.start();
        for (int i = 0; i < 5; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }

            System.out.println("курица!");
        }


        if (chiken.isAlive()) {
            try {
                chiken.join();
            } catch (InterruptedException e) {
            }

            System.out.println("Первым появилось яйцо!");
        } else {
            System.out.println("Первой появилась курица!");
        }
        System.out.println("Спор закончен!");
    }
}