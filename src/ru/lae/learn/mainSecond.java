package ru.lae.learn;

import java.io.*;
import java.util.ArrayList;


/**
 * Калькулятор который принимает значение из файла
 *
 * @author Lukashevich A. 17it18
 */
public class mainSecond {
    public static void main(String[] args) {
         ArrayList input = read("src\\Game\\txt.txt");
        String str = "";
        int[] split = new int[input.size()];
        for (int i = 0; i < input.size(); i++) {
            str = (String) input.get(i);
            String[] arr = str.split(" ");
            try {
                split = new int[]{Integer.parseInt(arr[0]), Integer.parseInt(arr[2])};
            } catch (NumberFormatException e) {
                input.set(i, "Неверный формат");
            }
            input.set(i, MathInteger.count(split[0], split[1],arr[1]));
        }
        write(input);
    }

    /**
     * Метод который берёт значения из файла и записывает из в arraylist
     * @param url - ссылка на файл
     * @return возвращяет заполненый данными из файла arraylist
     */
    static ArrayList read(String url) {
        String string;
        ArrayList<String> arrayList = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(url))) {
            while ((string = bufferedReader.readLine()) != null) {
                arrayList.add(string);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    /**
     * Записывает результат в файл из arraylist
     *
     * @param num - принимает arraylist с результатми
     */
    private static void write(ArrayList num) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\Game\\txt1.txt", false))) {
            for (int i = 0; i < num.size(); i++) {
                bufferedWriter.write(num.get(i) + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}