package ru.lae.learn;

public class MathInteger {
    /**
     * Принимает два числа и складывает их
     *
     * @param x - первое число
     * @param y - второе число
     * @return - результат
     */
    public static int sum(int x, int y) {
        int sum = 0;
        try {
            sum = x + y;
        } catch (Exception e) {
            System.out.println("Что-то пошло не так ведь тут целые числа"); // Выкинуть в main и там обработать, сделать корректные исключения
        }
        return sum;

    }

    /**
     * Принимает два числа и умножает их
     *
     * @param x - первое число
     * @param y - второе число
     * @return - результат
     */
    public static int multip(int x, int y) {
        int multip = 0;
        try {
            multip = x * y;
        } catch (Exception e) {
            System.out.println("Что-то пошло не так ведь тут целые числа");
        }
        return multip;
    }

    /**
     * Принимает два числа и возводдит в первое число в степень раную второму числу
     *
     * @param x - первое число
     * @param y - второе число (степень)
     * @return - результат
     */
    public static int pow(int x, int y) {
        int pow = 1;
        try {
            if (x < 0 || y < 0) {
                throw new Error("Отрицательные числа");
            }
            if (x == 0) {
                return 0;
            }
            if (y == 0) {
                return 1;
            }
            for (int i = 0; i < y; i++) {
                pow = pow * x;

            }
        } catch (Error e) {
            System.out.println("Отрицательное число");
        } catch (Exception e) {
            System.out.println("Что-то пошло не так ведь тут целые числа и не отричательные");
        }
        return pow;
    }

    /**
     * Принимает два числа и вычитает из первого числа второе
     *
     * @param x - первое число
     * @param y - второе число
     * @return - результат
     */
    public static int minus(int x, int y) {
        int minus = 0;
        try {
            minus = x - y;
        } catch (Exception e) {
            System.out.println("Что-то пошло не так ведь тут целые числа и не отричательные");
        }
        return minus;
    }

    /**
     * Принимает два числа и делит первое чило на второе
     *
     * @param x - первое число(Делимое)
     * @param y - второе число(Делитель)
     * @return - результат
     */
    public static int division(int x, int y) {
        int division = 0;
        try {
            division = x / y;
        } catch (Exception e) {
            System.out.println("Что-то пошло не так ведь тут целе числа и не отричательные");
        }
        return division;
    }

    /**
     * Принимает два числа и делит первое на второе и находит остаток
     *
     * @param x - первое число(Делимое)
     * @param y - второе число(Делитель)
     * @return - результат(Остаток)
     */
    public static int divisionEnd(int x, int y) {
        int divisionEnd = 0;
        try {
            divisionEnd = x % y;
        } catch (Exception e) {
            System.out.println("Что-то пошло не так ведь тут целе числа и не отричательные");
        }
        return divisionEnd;
    }

    /**
     * Метод который исходя из знака определяет что делать с числами
     * @param num1 - первое число
     * @param num2 - второе число
     * @param sign - знак
     * @return - возврат результата
     */

    public static int count(int num1, int num2, String sign) {
        int result = 0;
        switch (sign) {
            case ("/"):
                int div = MathInteger.division(num1, num2);
                result = div;
                break;
            case ("^"):
                int pow = MathInteger.pow(num1, num2);
                result = pow;
                break;
            case ("+"):
                int sum = MathInteger.sum(num1, num2);
                result = sum;
                break;
            case ("-"):
                int minus = MathInteger.minus(num1, num2);
                result = minus;
                break;
            case ("*"):
                int mult = MathInteger.multip(num1, num2);
                result = mult;
                break;
            case ("%"):
                int divEnd = MathInteger.divisionEnd(num1, num2);
                result = divEnd;
                break;
        }
        return result;
    }
}