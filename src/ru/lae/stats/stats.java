package ru.lae.stats;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class stats {

    /**
     * считывает данные из файла и записывает их в arraylist
     *
     * @param url - ссылка на файл
     * @return - arraylist с данными из файла
     */
    static ArrayList read(String url) {
        String string;
        ArrayList<String> arrayList = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(url))) {
            while ((string = bufferedReader.readLine()) != null) {
                arrayList.add(string);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    static int result = 0;
    static String[] arr;

    /**
     * Считает сколько символов в файле
     *
     * @param listArr - примимает arraylist c даными из файла
     * @return - возвращяет кол-во символов
     */
    static int statsSymble(ArrayList listArr) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < listArr.size(); i++) {
            arr = ((String) listArr.get(i)).split("");
            for (int j = 0; j < arr.length; j++) {
                arrayList.add(arr[j]);
            }
        }
        return result = arrayList.size();
    }

    /**
     * Считает сколько символов (без учёта пробелов) в файле
     * @param listArr - примимает arraylist c даными из файла
     * @return - возвращяет кол-во символов(без учёта пробелов)
     */
    static int statsSymbleNoSpace(ArrayList listArr) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < listArr.size(); i++) {
            arr = ((String) listArr.get(i)).split("");
            for (int j = 0; j < arr.length; j++) {
                arrayList.add(arr[j]);
            }
        }
        arrayList.removeAll(Arrays.asList(" "));
        return result = arrayList.size();
    }

    /**
     * Считает сколько слов в файле
     * @param listArr - примимает arraylist c даными из файла
     * @return - возвращяет кол-во слов
     */
    static int statsWord(ArrayList listArr) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < listArr.size(); i++) {
            arr = ((String) listArr.get(i)).split(" ");
            for (int j = 0; j < arr.length; j++) {
                arrayList.add(arr[j]);
            }
        }
        arrayList.removeAll(Arrays.asList(         //Удаляет всё что не должно считать за слово
                "!" , "@" , "#" , "$" , "%" ,
                "^" , "&" , "*" , "(" , ")" ,
                "+" , "-" , "_" , "=" , ";" ,
                "№" , ":" , "?" , "{" , "}" ,
                "[" , "]" , "/" , "." , "|" ,
                ">" , "<" , ","
        ));
        return result = arrayList.size();
    }

    /**
     * Записывает результата в файл
     * @param name - Для удобного вывода в файл названия какого параметра записывается
     * @param stats - Для вывода статитики
     */
    static void write(String name, int stats) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\Game\\txt1.txt", true))) {
            bufferedWriter.write(name + stats + "\n");
            System.out.println(name + stats);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
