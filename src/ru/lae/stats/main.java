package ru.lae.stats;

/**
 * @author Lukashevich A. 17it18
 */
public class main {
    public static void main(String[] args) {

        final String FILE = "src\\Game\\txt.txt";

        stats.write("Кол-во слов - ", stats.statsWord(stats.read(FILE)));
        stats.write("Кол-во символов - ", stats.statsSymble(stats.read(FILE)));
        stats.write("Кол-во символов без пробелов - ", stats.statsSymbleNoSpace(stats.read(FILE)));

    }
}
