package ru.lae.Obfuscator;

public interface Regulars {
    String COMMENTS ="\\/\\*.*?\\*\\/";
    String SPACES = "\\s{2,}";
    String CLASS = "(?<=class\\s)\\w+";
    String NAMEVARIABLES="(?<=(\\Wbyte\\s)|\\Wbyte\\[\\]\\s)\\w+|(?<=(\\Wshort\\s)|\\Wshort\\[\\]\\s)\\w+|(?<=(\\Wint\\s)|\\Wint\\[\\]\\s)\\w+|(?<=(\\Wlong\\s)|\\Wlong\\[\\]\\s)\\w+|(?<=(\\Wfloat\\s)|\\Wfloat\\[\\]\\s)\\w+|(?<=(\\Wdouble\\s)|\\Wdouble\\[\\]\\s)\\w+|(?<=(\\Wchar\\s)|\\Wchar\\[\\]\\s)\\w+|(?<=(\\Wboolean\\s)|\\Wboolean\\[\\]\\s)\\w+|(?<=(\\WString\\s)|\\WString\\[\\]\\s)\\w+";
    String NAMEMETHODS="(?<=(\\Wvoid\\s))(?!main)\\w+(?=\\()|(?<=(\\Wint\\s)|\\Wint\\[\\]\\s)\\w+(?=\\()|(?<=\\W\\>\\s)\\w+(?=\\()|(?<=\\w\\>\\s)\\w+(?=\\()|(?<=(\\Wbyte\\s)|\\Wbyte\\[\\]\\s)\\w+(?=\\()|(?<=(\\Wshort\\s)|\\Wshort\\[\\]\\s)\\w+(?=\\()|(?<=(\\Wlong\\s)|\\Wlong\\[\\]\\s)\\w+(?=\\()|(?<=(\\Wfloat\\s)|\\Wfloat\\[\\]\\s)\\w+(?=\\()|(?<=(\\Wdouble\\s)|\\Wdouble\\[\\]\\s)\\w+(?=\\()|(?<=(\\Wchar\\s)|\\Wchar\\[\\]\\s)\\w+(?=\\()|(?<=(\\Wboolean\\s)|\\Wboolean\\[\\]\\s)\\w+(?=\\()|(?<=(\\WString\\s)|\\WString\\[\\]\\s)\\w+(?=\\()";
}