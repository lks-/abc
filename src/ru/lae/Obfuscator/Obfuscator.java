package ru.lae.Obfuscator;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс удаляет  коменнтарии,
 * двойные и более пробелы,
 * заменяет имена переменных,
 * класса на набор символов
 *
 * @author Lukashevich А.
 */

public class Obfuscator implements Regulars {
    private String code;
    private String url;

    /**
     *Конструктор класса с аргументом на путь к файлу
     *
     * @param url - путь к файлу который нужно изменить
     */
    Obfuscator(String url) {
        this.url = url;
    }

    /**
     * Метод для запуска обфускатора
     */
    void statrObfuscator() {
        code = ReadFile.read(url);
        deleteElements(COMMENTS);
        deleteElements(SPACES);
        renameClass();
        replacementIdentifiers(serchIdentifiers(NAMEVARIABLES));
        replacementIdentifiers(serchIdentifiers(NAMEMETHODS));
        WriteFile.write(code, url);
    }

    /**
     * Метод для удаления элементов кода по регулярке
     *
     * @param regex - регулярка для удаления
     */
    private void deleteElements(String regex) {
        Pattern pattern = Pattern.compile(regex);
        code = code.replaceAll(String.valueOf(pattern), "");
    }

    /**
     * Метод для поиска имени класса и замены его на другое
     */
    private void renameClass() {
        Pattern pattern = Pattern.compile(CLASS);
        Matcher matcher = pattern.matcher(code);
        String nameClass = "";
        while (matcher.find()) {
            nameClass = matcher.group();
        }
        String newName = randomWord();
        code = code.replaceAll(nameClass, newName);
        renameFile(nameClass, newName);
    }

    /**
     * Метод для создания рандомных слов из 3 букв английского языка
     *
     * @return - рандомное слово
     */
    private String randomWord() {
        StringBuilder word = new StringBuilder();

        char[] randName = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

        for (int i = 0; i < 3; i++) {
            Random random = new Random();
            word.append(randName[random.nextInt(26)]);
        }
        return word.toString();
    }

    /**
     * Метод для изменения имени обрабатываемого файла
     *
     * @param originalName - начально название файла
     * @param newName - новое имя файла
     */
    private void renameFile(String originalName, String newName) {
        File file = new File("src\\Game\\" + originalName + ".java");
        url = "src\\Game\\" + newName + ".java";
        file.renameTo(new File(url));
    }

    /**
     * Осуществляет поиск идентификаторов в коде и записывает их в arraylist
     */
    private ArrayList<String> serchIdentifiers(String regex) {
        ArrayList<String> nameIdentifiers = new ArrayList<>();
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(code);
        String name;
        while (matcher.find()) {
            name = matcher.group();
            nameIdentifiers.add(name);
        }
        nameIdentifiers = new ArrayList<>(new HashSet<>(nameIdentifiers));
        return nameIdentifiers;
    }

    /**
     * Заменяет идентификаторы в коде на новые
     */
    private void replacementIdentifiers(ArrayList<String> nameIndentifiers) {
        for (String nameVariable : nameIndentifiers) {
            String name = randomWord();
            code = code.replaceAll( "\\b"+nameVariable + "\\b", name);
        }
    }
}
