package ru.lae.Obfuscator;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс для чтения файла
 *
 * @author Lukashevich A.
 */
class ReadFile {
    /**
     * Осуществляет чтение файла в одну строку
     *
     * @param url - путь к файлу
     * @return - код записанный в одну строку
     */
    static String read(String url) {
        StringBuilder sB = new StringBuilder();

        try (BufferedReader bF = new BufferedReader(new FileReader(url))) {
            String line;
            while ((line = bF.readLine()) != null) {
                System.out.println(line);
                if (singleLineCommentCheck(line)) {
                    sB.append(line);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return sB.toString();
    }

    /**
     * Метод для проверки строки на однострочный комментарий
     *
     * @param line - строка для проверки
     * @return - true или false
     */
    private static boolean singleLineCommentCheck(String line) {
        boolean permission = true;
        Pattern pattern = Pattern.compile("//[^\\r\\n]+");
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()) {
            permission = false;
        }
        System.out.println(permission);

        return permission;
    }
}