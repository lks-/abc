package ru.lae.Obfuscator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Класс для записи данных в файл
 *
 * @author Lukashevich А.
 */
class WriteFile {
    /**
     * Метод для записи данных в файл
     *
     * @param str - текс который нужно записать
     * @param url - путь к файлу
     */
    static void write(String str, String url) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(url))) {
            bw.write(str);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
