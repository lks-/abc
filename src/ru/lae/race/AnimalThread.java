package ru.lae.race;

/**
 *
 * Метод имеет поля имени и приоритета и устанавливает их
 * Также он запускает потоки
 */
public class AnimalThread extends Thread {

    String name;
    int priority;

    AnimalThread (String name,int priority){
        this.name = name;
        this.priority = priority;
        setName(name);
        setPriority(priority);
        start();
    }

    /**
     * Метод начинает гонки потоков
     * Выводит каждый шаг потоков
     * Если поток доходит до определённого шага то приоритет определённого потока меняется
     */
    public void run(){
        for(int i = 0;i <= 100;i++){
            System.out.println(getName() + " " + i);
            if((i == 30)&&(name.equals("turtle"))){
                setPriority(10);
                System.out.println(getName() + getPriority() + "___________________________________");
            }
            if((i == 30)&&(name.equals("rabbit"))){
                setPriority(1);
                System.out.println(getName() + getPriority() + "__________________________________");
            }

        }
    }
}


