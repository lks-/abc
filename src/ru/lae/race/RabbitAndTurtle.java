package ru.lae.race;

/**
 * Догонялки потоков
 *
 * @author Lukashevich A. 17it18
 */
public class RabbitAndTurtle {
    public static void main(String[] args) {
        new AnimalThread("rabbit", 10);
        new AnimalThread("turtle", 1);

    }
}

