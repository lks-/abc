package ru.lae.copyFile;

/**
 * В отдельном потоке происходит копирование из одного файла в другой
 *
 * @author Lukashevich A.E.
 */
public class Main {
    public static void main(String[] args) {
        CopyThread copy = new CopyThread("src\\Game\\txt.txt");
        copy.run();
    }
}
