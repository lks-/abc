package ru.lae.copyFile;

import java.io.*;
import java.util.ArrayList;

/**
 * Класс потока который копирует данные из одиного файлф и записывает в другой
 */
public class CopyThread extends Thread {

    String url;

    CopyThread(String url){
        this.url = url;
    }

    @Override
    public void run() {
        write(read(url));
    }

    /**
     * Метод который считывает из файла
     * @param url - ссылка на файл
     * @return - возврат заполненого Arraylist
     */
    static ArrayList read(String url) {
        String string;
        ArrayList<String> arrayList = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(url))) {
            while ((string = bufferedReader.readLine()) != null) {
                arrayList.add(string);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    /**
     * Метод который записывает в файл
     * @param arrayList - заполненый ArrayList который записывается в файл
     */
    private static void write(ArrayList arrayList) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\Game\\txt1.txt", false))) {
            for (int i = 0; i < arrayList.size(); i++) {
                bufferedWriter.write(arrayList.get(i) + "\n");

            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
