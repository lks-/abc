package ru.lae.bankAccount;


/**
 * Класс который снимает деньги с баланса
 */
public class TakeThreat extends Thread {
    Account account;
    long money;

    /**
     * Конструктор который принимает поток аккаунта и сколько денег будет сниматься
     *
     * @param account - поток аккаунта
     * @param money - деньги которые будут сниматься
     */
    TakeThreat(Account account, long money){
        this.account = account;
        this.money = money;
    }

    @Override
    public void run(){
        while (true) {
            account.takeOff(money);
        }
    }
}
