package ru.lae.bankAccount;

/**
 * Класс который начисляет деньги
 */
public class AddingThreat extends Thread {
    Account account;
    long money, sleeping, i;

    /**
     * Конструктор который определяет сколько раз будет начисленно денег,
     * как будет называться поток и с какой переодичностью
     *
     * @param name - Имя потока
     * @param money - Сколько денег будет начисляться
     * @param i - Сколько раз будет начисленно денег
     * @param sleeping - С какой переодичностью деньги будут начисляться
     * @param account - Поток аккаунта
     */
    AddingThreat(String name, long money, long i, long sleeping,Account account) {
        setName(name);
        this.account = account;
        this.money = money;
        this.sleeping = sleeping;
        this.i = i;
    }

    @Override
    public void run() {
        for (int j = 1; j <= i; j++) {
                account.add(money);
                System.out.println("Пришла " + getName() + " " + money);
                try {
                    sleep(sleeping);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

