package ru.lae.bankAccount;

import java.util.Scanner;

/**
 * Приложение банка.
 * Создаётся аккаунт(либо с начальным балансом new Account(500), либо new Account())
 * На баланс этого акканта начисляются деньги и когда достигается определённая сумма, деньги снимаются
 *
 * get balance - Постомтреть баланс
 * exit - Выйти из приложения
 *
 * @author Lukashevich A.E
 */
public class Main {
    public static void main(String[] args) {
        Account account = new Account();
        Scanner in = new Scanner(System.in);
        String moveInBalance = "";
        boolean starts = true;
        System.out.println("Введите что вы хотите сделать?\n" +
                "get balance - Посмотреть баланс\n" +
                "exit - Закончить");
        AddingThreat zp = new AddingThreat("Зарплата",200,7,1000,account);
        TakeThreat take = new TakeThreat(account,300);
        zp.start();
        take.start();
        while (starts) {
            moveInBalance = in.nextLine();
            switch (moveInBalance) {
                case "get balance":
                    System.out.println(account.getBalance());
                    break;
                case "exit":
                    starts = false;
                    System.exit(1);
                    break;
            }
        }
    }
}
