package ru.lae.bankAccount;


/**
 * Класс аккаунта
 */
public class Account {
    private long balance;

    /**
     * Констуруктор аккаунта с начальными деньгами на балансе
     *
     * @param balance - баланс аккакнта
     */
    public Account(long balance) {
        this.balance = balance;
    }

    /**
     * Конструктор аккаунта с нулевым балансом
     */
    public Account() {
        this.balance = balance;
    }

    /**
     * Метод который добавляет денег на баланс
     * И запускает wait из метода
     *
     * @param addMoney - Столько денег будет начисленно
     */
     synchronized void add(long addMoney) {
        balance += addMoney;
        notify();
    }

    /**
     * Метод который снимает деньги с баланса
     * И проверяет сколько денег на балансе.
     * Если хватает для снятия то снимает, если нет то запускается метод wait() и поток ждёт выполение метода add
     *
     * @param delMoney - Столько денег будет снято
     */
    synchronized void takeOff(long delMoney) {

        while (balance < delMoney){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if(balance >= delMoney) {
            balance -= delMoney;
            System.out.println("Произошло списание " + delMoney);
        }

    }

    /**
     * Метод который вовращяет значение баланса
     *
     * @return - возврат балланса
     */
    public long getBalance() {
        return balance;
    }

}
