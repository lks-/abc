package ISprac;

import java.util.Scanner;

/**
 * Программма проверяет в полученном числе каждую цифру и выводит самую большую цифру кратную трём,
 * если таких нет то выводит NO
 *
 * @author Lukashevich A.
 */
public class IS {
    private int num;
    private int maxDigit;
    private Boolean start = true;

    /**
     * Класс который запускает все методы
     * @param args .
     */
    public static void main(String[] args) {
        IS is = new IS();
        Scanner in = new Scanner(System.in);
        int checkNum = in.nextInt();
        is.launch(checkNum);
    }

    /**
     * Метод который проверяет число полученное число на допустивое значение,
     * Если проходит проверку то запускает метод count()
     *
     * @param i Число которое проверяется
     * @return - результат
     */
    public int launch(int i) {
        Scanner in = new Scanner(System.in);
        num = i;
        while (start) {
            if (num > Math.pow(10, 9)) {
                System.out.println("Вы ввели слишком большое число\n" + "Попробуйте ещё раз");
                num = in.nextInt();
            } else if (num < 0) {
                System.out.println("Вы ввели отрицательно число\n" + "Попробуйте ещё раз");
                num = in.nextInt();
            } else {
                count();
            }
        }
        return maxDigit;
    }

    /**
     * Метод который перебирает переменную и проверяет каждую цифру
     */
    private void count() {
        while (num != 0) {
            int digit = num % 10;
            if (digit % 3 == 0)
                if (digit > maxDigit)
                    maxDigit = digit;
            num = num / 10;
            System.out.println(num);
        }
        if (maxDigit == 0) {
            System.out.println("NO");
            start = false;
        } else {
            System.out.println(maxDigit);
            start = false;
        }
    }
}



